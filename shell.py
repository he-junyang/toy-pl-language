"""
主窗口, 入口文件
"""
import sys

import basic


def exec_fn(path):
    try:
        with open(path, 'r', encoding='utf-8') as f:
            script = f.read()
    except Exception as e:
        print(f'加载脚本 {path} 失败, 错误: {e}')
        raise

    result, error = basic.run(path, script)
    if error:
        print(error.as_string())


def shell():
    while True:
        """
        不断接收键盘输入, 交给词法分析器进行处理
        """
        text = input('basic > ')
        if text == 'exit':
            print('bye')
            break
        res, error = basic.run('<stdin>', text)
        if error:
            print(error.as_string())
        else:
            print(res)


if __name__ == '__main__':

    if len(sys.argv) > 1:
        fn_path = sys.argv[1]
        exec_fn(fn_path)
    else:
        shell()
