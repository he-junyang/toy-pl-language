# ToyPL
toy programmer language 玩具编程语言

示例如下: 
![show](./img/example.png)
```shell
py .\shell.py
basic > var name = "hello world"
[hello world]
basic > name
[hello world]
basic > exit
bye
```
```shell
py .\shell.py .\test_toypl.pl
# [1, 1, 2, 3, 5, 8, 13, 21, 34, 55]
```

shell.py : 主入口

basic.py : 其中的run方法执行了词法分析,语法分析,解释器执行

lexer.py : 词法分析相关的实现

tokens.py : 词法分析生成的 token 对应的类, 还有其他相关类的定义

parser.py : 语法分析相关的实现, 形成 ast抽象语法树

interpreter.py : 解释器执行相关的实现, 将语法分析生成的AST解释执行

error.py : 错误类定义

symbol_table.py : 符号表的顶用, 也就是存储变量的值, 全局有一个符号表, 存储咋 Context 上下文中(basic.py)

ast_node.py : 语法分析生成的节点

context.py : 上下文类定义

function.py : 函数包裹类和内建函数定义

result.py : 定义了 ParseResult 和 RTResult 结果类

test.py : 测试文件

test_toypl.pl : ToyPL 语言编写的文件

type_operate.py : 结果包装类, 如 Number, String, List 等等

文法定义如下: 
```text
KEYWORD : var , not ,and ,or, if, elif, then,else, for, step, while, to, func
IDENTIFIER : 变量
EQ : =
POW : ^ 幂符号
COMMA : 逗号 ,
ARROW : -> 箭头
STRING : 字符串
comp-expr: 比较运算式
arith-expr: 算数运算式
if-expr: if 判断
for-expr: for 循环
while-expr: while 循环
EE: ==    LT: <    GT: >    LTE: <=    GTE: >=    NE: ==
```
```text
statements => NEWLINE* statement (NEWLINE+ statement)* NEWLINE*

statement => KEYWORD:return expr?
          => KEYWORD:continue
          => KEYWORD:break
          => expr

expr => KEYWORD:var IDENTIFIER EQ expr
     => comp-expr (( KEYWORD:and | KEYWORD:or) comp-expr)*
     
comp-expr => KEYWORD:not comp-expr # 取反
          => arith-expr ((EE | LT | GT | LTE | GTE | NE) arith-expr)*
          
arith-expr => term ((PLUS | MINUS) term)* 

term => factor ((MUL | DIV) factor)*

factor => (PLUS | MINUS) factor # 正数与负数
       => power
       
power => call (POW factor)*

call => atom (LPAREN (expr (COMMA expr)*)? RPAREN)? # 调用函数或者调用变量, 如 atop(1,2) , atom

atom => INT | FLOAT | STRING | IDENTIFIER
     => LPAREN expr RPAREN
     => if-expr
     => list-expr
     => for-expr
     => while-expr
     => func-expr
     
list-expr =>  LSQUARE (expr (COMMA expr)*)? RSQUARE # [1,2,3]

if-expr => KEYWORD:if statement KEYWORD:then 
           (( expr (if-expr-elif | if-expr-else?) ) | (NEWLINE statements (KEYWORD:end | if-expr-elif | if-expr-else)) )


if-expr-elif => KEYWORD:elif statement KEYWORD:then
                ((expr (if-expr-elif | if-expr-else?)) | (NEWLINE statements (KEYWORD:end | if-expr-elif | if-expr-else)) )

if-expr-else => KEYWORD:else
                (statement | (NEWLINE statements KEYWORD:end))
        
for-expr => KEYWORD:for IDENTIFIER EQ expr KEYWORD:to expr
            (KEYWORD:step expr)? KEYWORD:then 
            (statement | (NEWLINE statements KEYWORD:end))

while-expr => KEYWORD:while expr KEYWORD:then
              (statement | (NEWLINE statements KEYWORD:end))

func-expr => KEYWORD:func IDENTIFIER?
             LPAREN (IDENTIFIER (COMMA IDENTIFIER)*)? RPAREN
             ((ARROW expr) | (NEWLINE statements KEYWORD:end))
```


## 1. 词法分析器
1. 实现词法分析器, 首先实现 基本算数语句的词法分析
   1. 示例: 1 * 1 + 2 + (1 * 10)
   2. 生成tokens:`[INT 1,MUL,INT 1,PLUS,INT 2,LPAREN,INT 1,MUL,INT 10,RPAREN]`

核心方法 Lexer.make_number, 将 token 处理成了列表


## 2. 语法分析器
将词法分析器Lexer 解析的 tokens 转换为 AST(抽象语法树)

我们使用上下文无关语法BNF来表达语言的规则

我们实现一个四则运算的文法:

文法是通过递归实现的, 所以 factor 对应的文法优先级高于 term 和 expr, 同样的, term 对应的文法优先级高于 expr, 这样就实现了乘除优先级高于加减, 括号优先级最高
```text
expr => term ((PLUS | MINUS) term)*
term => factor(( MUL | DIV) factor)*
factor => INT | FLOAT # 浮点数或者整数
       => ( PLUS | MINUS ) factor # 正数或者是负数
       => LPAREN expr RPAREN # 括号
表示为基本文法形式   
G[S]: // * 表示上一个括号的内容重复零次或多次, 括号内的 | 表示这个|左右的内容可以任选
      // a, b, c, d, e, f: 整数, 浮点数, 加号, 减号, 左圆括号, 右圆括号
S => A((+|-)A)*
A => B((*|/)B)*
B => a|b
  => (c|d)B
  => eSf
```
对 (1 + 1) * 2
```text
S => A => B * B=> eSf * B => (S) * B=> (A + A) * B
 => (B + A) * B => (1 + A) * B => ... => (1 + 1) * 2
```
生成的 AST 为 `((INT: 1, PLUS, INT: 1), MUL, INT: 2)`

核心方法 Parser.parse , 将 token 根据文法处理成了一个AST树

## 3. 解释器
解释器已经到了最后一步, 语法分析器生成的AST通过解释器就可以等到结果了, 我们的算数运算器可以完工了

AST => interpreter => RTResult(RunTime Result)得到结果

传入 AST 树根, 递归执行结果

核心方法 Interpreter.visit 将AST抽象语法树树根 Node 执行为四则运算结果 

## 4. 添加幂运算与变量
幂运算: `x^y = x的y次方`

变量: `var a = 1 + 3`

keyword = var

变量名 = a

赋值操作符 = '='

1. 修改语法规则
```text
KEYWORD : var
IDENTIFIER : 变量
EQ : =
POW : ^ 幂符号

expr => KEYWORD IDENTIFIER EQ expr
     => term ((PLUS | MINUS) term)*
     
term => factor ((MUL | DIV) factor)*

factor => (PLUS | MINUS) factor # 正数与负数
       => power
power => atom (POW factor)*

atom => INT | FLOAT | IDENTIFIER
     => LPAREN expr RPAREN
```
2. 添加 tokens
3. 添加 ast node , 现在又新增了
4. 4修改词法分析器规则
5. 修改语法分析器规则
6. 修改解释器


现在我们引入了变量的概念, 那么问题来了, 我们该如何给变量赋值?

我们可以引入一个类符号表 symbol table , 我们在符号表中用 dict 存储变量对应的值

为了体现出作用域的关系, 我们可以设置一个 parent 属性用于连接父级的符号表
,全局有一个符号表, 单独的方法也有符号表
示例: 
```shell
basic > >? var a = 10
词法解析器生成的tokens:  [KEYWORDS: var, IDENTIFIER: a, EQ, INT: 10, EOF]
语法解析器生成的AST抽象语法树:  IDENTIFIER: a, INT: 10
10
basic > >? var b = var c = 1 + 10
词法解析器生成的tokens:  [KEYWORDS: var, IDENTIFIER: b, EQ, KEYWORDS: var, IDENTIFIER: c, EQ, INT: 1, PLUS, INT: 10, EOF]
语法解析器生成的AST抽象语法树:  IDENTIFIER: b, IDENTIFIER: c, (INT: 1, PLUS, INT: 10)
11
basic > 
```

## 5. 添加比较运算符和逻辑运算符

添加 `<` `<=` `>` `>=` `==` `!=` 等比较运算符

添加 `and` `or` `not` 等逻辑运算符

文法规则: 
```text
KEYWORD : var , not ,and ,or
IDENTIFIER : 变量
EQ : =
POW : ^ 幂符号
comp-expr: 比较运算式
arith-expr: 算数运算式
EE: ==    LT: <    GT: >    LTE: <=    GTE: >=    NE: ==

expr => KEYWORD:var IDENTIFIER EQ expr
     => comp-expr (( KEYWORD:and | KEYWORD:or) comp-expr)*
     
comp-expr => KEYWORD:not comp-expr # 取反
          => arith-expr ((EE | LT | GT | LTE | GTE | NE) arith-expr)*
          
arith-expr => term ((PLUS | MINUS) term)* 

term => factor ((MUL | DIV) factor)*

factor => (PLUS | MINUS) factor # 正数与负数
       => power
       
power => atom (POW factor)*

atom => INT | FLOAT | IDENTIFIER
     => LPAREN expr RPAREN
```

我们根据更新后的文法规则更新 常量, 词法分析器 , 语法分析器, 解释器
示例: 
```shell
basic > >? 1 == 2
词法解析器生成的tokens:  [INT: 1, EE, INT: 2, EOF]
语法解析器生成的AST抽象语法树:  (INT: 1, EE, INT: 2)
0
basic > >? 1 == 1
词法解析器生成的tokens:  [INT: 1, EE, INT: 1, EOF]
语法解析器生成的AST抽象语法树:  (INT: 1, EE, INT: 1)
1
basic > >? var a = 2
词法解析器生成的tokens:  [KEYWORDS: var, IDENTIFIER: a, EQ, INT: 2, EOF]
语法解析器生成的AST抽象语法树:  IDENTIFIER: a, INT: 2
2
basic > >? a == 2 and 3 >= 1 + 1
词法解析器生成的tokens:  [IDENTIFIER: a, EE, INT: 2, KEYWORDS: and, INT: 3, GTE, INT: 1, PLUS, INT: 1, EOF]
语法解析器生成的AST抽象语法树:  ((IDENTIFIER: a, EE, INT: 2), KEYWORDS: and, (INT: 3, GTE, (INT: 1, PLUS, INT: 1)))
1
basic > 
```

## 5. 添加if判断

我们添加的 if 判断格式如下: 

if ... then ...

elif ... then ...

elif ... then ...

else ...

end

1. 修改常量 
2. 添加 ast node ifNode 
3. 修改语法解析器
4. 修改解析器, 支持 ifNode

新的文法系统如下: 
```text
KEYWORD : var , not ,and ,or
IDENTIFIER : 变量
EQ : =
POW : ^ 幂符号
comp-expr: 比较运算式
arith-expr: 算数运算式
if-expr: if 判断
EE: ==    LT: <    GT: >    LTE: <=    GTE: >=    NE: ==

expr => KEYWORD:var IDENTIFIER EQ expr
     => comp-expr (( KEYWORD:and | KEYWORD:or) comp-expr)*
     
comp-expr => KEYWORD:not comp-expr # 取反
          => arith-expr ((EE | LT | GT | LTE | GTE | NE) arith-expr)*
          
arith-expr => term ((PLUS | MINUS) term)* 

term => factor ((MUL | DIV) factor)*

factor => (PLUS | MINUS) factor # 正数与负数
       => power
       
power => atom (POW factor)*

atom => INT | FLOAT | IDENTIFIER
     => LPAREN expr RPAREN
     => if-expr

if-expr => KEYWORD:if expr KEYWORD:then expr
           (KEYWORD:elif expr KEYWORD:then expr)* # 0次或多次
           (KEYWORD:else expr)? # 0次或1次
```

示例: 
```shell
basic > >? if 1 == 1 then var a = 3 elif 1 > 2 then var b = 3 else var c = 3
词法解析器生成的tokens:  [KEYWORDS: if, INT: 1, EE, INT: 1, KEYWORDS: then, KEYWORDS: var, IDENTIFIER: a, EQ, INT: 3, KEYWORDS: elif, INT: 1, GT, INT: 2, KEYWORDS: then, KEYWORDS: var, IDENTIFIER: b, EQ, INT: 3, KEYWORDS: else, KEYWORDS: var, IDENTIFIER: c, EQ, INT: 3, EOF]
语法解析器生成的AST抽象语法树:  (if (INT: 1, EE, INT: 1) then IDENTIFIER: a, INT: 3 elif (INT: 1, GT, INT: 2) then IDENTIFIER: b, INT: 3 else IDENTIFIER: c, INT: 3)
[3, '判断值为 False', 3]
basic > >? a
词法解析器生成的tokens:  [IDENTIFIER: a, EOF]
语法解析器生成的AST抽象语法树:  IDENTIFIER: a
3
basic > >? c
词法解析器生成的tokens:  [IDENTIFIER: c, EOF]
语法解析器生成的AST抽象语法树:  IDENTIFIER: c
3
basic > 
```

## 6. 添加循环
形式如下: 
```java
for i = 1 to 10 step 2 then
   
   var res = res + i

end   
for i = 1 to 10 then
   
   var res = res + i
end
   
while i < 10 then
   var res = res + 1
   var i = i + 1
end
```


文法如下
```text
KEYWORD : var , not ,and ,or, if, elif, then,else, for, step, while, to
IDENTIFIER : 变量
EQ : =
POW : ^ 幂符号
comp-expr: 比较运算式
arith-expr: 算数运算式
if-expr: if 判断
for-expr: for 循环
while-expr: while 循环
EE: ==    LT: <    GT: >    LTE: <=    GTE: >=    NE: ==

expr => KEYWORD:var IDENTIFIER EQ expr
     => comp-expr (( KEYWORD:and | KEYWORD:or) comp-expr)*
     
comp-expr => KEYWORD:not comp-expr # 取反
          => arith-expr ((EE | LT | GT | LTE | GTE | NE) arith-expr)*
          
arith-expr => term ((PLUS | MINUS) term)* 

term => factor ((MUL | DIV) factor)*

factor => (PLUS | MINUS) factor # 正数与负数
       => power
       
power => atom (POW factor)*

atom => INT | FLOAT | IDENTIFIER
     => LPAREN expr RPAREN
     => if-expr
     => for-expr
     => while-expr

if-expr => KEYWORD:if expr KEYWORD:then expr
           (KEYWORD:elif expr KEYWORD:then expr)* # 0次或多次
           (KEYWORD:else expr)? # 0次或1次
           
for-expr => KEYWORD:for IDENTIFIER EQ expr KEYWORD:to expr
            (KEYWORD:step expr)? KEYWORD:then expr

while-expr => KEYWORD:while expr KEYWORD:then expr
```

测试代码在 test.py 中

## 7. 添加函数

func

```text
func add(a,b) -> a + b

func -> a + b

func add() -> 1 + 2
```
最终语法
```text
for i = 1 to n+1 step 1 then
    var res_list = append(res_list, Fib(i))
end
```

注意, 每个函数有其独特的作用域, 所以有新的上下文

```text
KEYWORD : var , not ,and ,or, if, elif, then,else, for, step, while, to, func
IDENTIFIER : 变量
EQ : =
POW : ^ 幂符号
COMMA : 逗号 ,
ARROW : -> 箭头
comp-expr: 比较运算式
arith-expr: 算数运算式
if-expr: if 判断
for-expr: for 循环
while-expr: while 循环
EE: ==    LT: <    GT: >    LTE: <=    GTE: >=    NE: ==

expr => KEYWORD:var IDENTIFIER EQ expr
     => comp-expr (( KEYWORD:and | KEYWORD:or) comp-expr)*
     
comp-expr => KEYWORD:not comp-expr # 取反
          => arith-expr ((EE | LT | GT | LTE | GTE | NE) arith-expr)*
          
arith-expr => term ((PLUS | MINUS) term)* 

term => factor ((MUL | DIV) factor)*

factor => (PLUS | MINUS) factor # 正数与负数
       => power
       
power => call (POW factor)*

call => atom (LPAREN (expr (COMMA expr)*)? RPAREN)? # 调用函数或者调用变量, 如 atop(1,2) , atom

atom => INT | FLOAT | IDENTIFIER
     => LPAREN expr RPAREN
     => if-expr
     => for-expr
     => while-expr
     => func-expr

if-expr => KEYWORD:if expr KEYWORD:then expr
           (KEYWORD:elif expr KEYWORD:then expr)* # 0次或多次
           (KEYWORD:else expr)? # 0次或1次
           
for-expr => KEYWORD:for IDENTIFIER EQ expr KEYWORD:to expr
            (KEYWORD:step expr)? KEYWORD:then expr

while-expr => KEYWORD:while expr KEYWORD:then expr

func-expr => KEYWORD:func IDENTIFIER?
             LPAREN (IDENTIFIER (COMMA IDENTIFIER)*)? RPAREN
             ARROW expr
```


## 8. 添加字符串

```txt
var s = "String"
var s = "String \" x \" "
var s = "Str \n ing \t xxx"
```

文法: 
```text
KEYWORD : var , not ,and ,or, if, elif, then,else, for, step, while, to, func
IDENTIFIER : 变量
EQ : =
POW : ^ 幂符号
COMMA : 逗号 ,
ARROW : -> 箭头
STRING : 字符串
comp-expr: 比较运算式
arith-expr: 算数运算式
if-expr: if 判断
for-expr: for 循环
while-expr: while 循环
EE: ==    LT: <    GT: >    LTE: <=    GTE: >=    NE: ==

expr => KEYWORD:var IDENTIFIER EQ expr
     => comp-expr (( KEYWORD:and | KEYWORD:or) comp-expr)*
     
comp-expr => KEYWORD:not comp-expr # 取反
          => arith-expr ((EE | LT | GT | LTE | GTE | NE) arith-expr)*
          
arith-expr => term ((PLUS | MINUS) term)* 

term => factor ((MUL | DIV) factor)*

factor => (PLUS | MINUS) factor # 正数与负数
       => power
       
power => call (POW factor)*

call => atom (LPAREN (expr (COMMA expr)*)? RPAREN)? # 调用函数或者调用变量, 如 atop(1,2) , atom

atom => INT | FLOAT | STRING | IDENTIFIER
     => LPAREN expr RPAREN
     => if-expr
     => for-expr
     => while-expr
     => func-expr

if-expr => KEYWORD:if expr KEYWORD:then expr
           (KEYWORD:elif expr KEYWORD:then expr)* # 0次或多次
           (KEYWORD:else expr)? # 0次或1次
           
for-expr => KEYWORD:for IDENTIFIER EQ expr KEYWORD:to expr
            (KEYWORD:step expr)? KEYWORD:then expr

while-expr => KEYWORD:while expr KEYWORD:then expr

func-expr => KEYWORD:func IDENTIFIER?
             LPAREN (IDENTIFIER (COMMA IDENTIFIER)*)? RPAREN
             ARROW expr
```


## 9. 添加列表

list 列表

[1, 2, 3]

[1,2,3] + 4 => [1,2,3,4]

[1,2,3] + [4,5,6] => [1,2,3,[4,5,6]]

[1,2,3] / 0 => 1 

[1,2,3] * [4,5,6] => [1,2,3,4,5,6]

[1,2,3] - 0 => [2,3]


文法: 
```text
KEYWORD : var , not ,and ,or, if, elif, then,else, for, step, while, to, func
IDENTIFIER : 变量
EQ : =
POW : ^ 幂符号
COMMA : 逗号 ,
ARROW : -> 箭头
STRING : 字符串
comp-expr: 比较运算式
arith-expr: 算数运算式
if-expr: if 判断
for-expr: for 循环
while-expr: while 循环
EE: ==    LT: <    GT: >    LTE: <=    GTE: >=    NE: ==

expr => KEYWORD:var IDENTIFIER EQ expr
     => comp-expr (( KEYWORD:and | KEYWORD:or) comp-expr)*
     
comp-expr => KEYWORD:not comp-expr # 取反
          => arith-expr ((EE | LT | GT | LTE | GTE | NE) arith-expr)*
          
arith-expr => term ((PLUS | MINUS) term)* 

term => factor ((MUL | DIV) factor)*

factor => (PLUS | MINUS) factor # 正数与负数
       => power
       
power => call (POW factor)*

call => atom (LPAREN (expr (COMMA expr)*)? RPAREN)? # 调用函数或者调用变量, 如 atop(1,2) , atom

atom => INT | FLOAT | STRING | IDENTIFIER
     => LPAREN expr RPAREN
     => if-expr
     => list-expr
     => for-expr
     => while-expr
     => func-expr
     
list-expr =>  LSQUARE (expr (COMMA expr)*)? RSQUARE # [1,2,3]

if-expr => KEYWORD:if expr KEYWORD:then expr
           (KEYWORD:elif expr KEYWORD:then expr)* # 0次或多次
           (KEYWORD:else expr)? # 0次或1次
           
for-expr => KEYWORD:for IDENTIFIER EQ expr KEYWORD:to expr
            (KEYWORD:step expr)? KEYWORD:then expr

while-expr => KEYWORD:while expr KEYWORD:then expr

func-expr => KEYWORD:func IDENTIFIER?
             LPAREN (IDENTIFIER (COMMA IDENTIFIER)*)? RPAREN
             ARROW expr
```

## 10. 添加内建函数

添加一些内置的函数, 如 print, append 等等

## 11. 添加多行编程, return break continue 语句

最后的一项
```text
KEYWORD : var , not ,and ,or, if, elif, then,else, for, step, while, to, func
IDENTIFIER : 变量
EQ : =
POW : ^ 幂符号
COMMA : 逗号 ,
ARROW : -> 箭头
STRING : 字符串
comp-expr: 比较运算式
arith-expr: 算数运算式
if-expr: if 判断
for-expr: for 循环
while-expr: while 循环
EE: ==    LT: <    GT: >    LTE: <=    GTE: >=    NE: ==
```
```text
statements => NEWLINE* statement (NEWLINE+ statement)* NEWLINE*

statement => KEYWORD:return expr?
          => KEYWORD:continue
          => KEYWORD:break
          => expr

expr => KEYWORD:var IDENTIFIER EQ expr
     => comp-expr (( KEYWORD:and | KEYWORD:or) comp-expr)*
     
comp-expr => KEYWORD:not comp-expr # 取反
          => arith-expr ((EE | LT | GT | LTE | GTE | NE) arith-expr)*
          
arith-expr => term ((PLUS | MINUS) term)* 

term => factor ((MUL | DIV) factor)*

factor => (PLUS | MINUS) factor # 正数与负数
       => power
       
power => call (POW factor)*

call => atom (LPAREN (expr (COMMA expr)*)? RPAREN)? # 调用函数或者调用变量, 如 atop(1,2) , atom

atom => INT | FLOAT | STRING | IDENTIFIER
     => LPAREN expr RPAREN
     => if-expr
     => list-expr
     => for-expr
     => while-expr
     => func-expr
     
list-expr =>  LSQUARE (expr (COMMA expr)*)? RSQUARE # [1,2,3]

if-expr => KEYWORD:if statement KEYWORD:then 
           (( expr (if-expr-elif | if-expr-else?) ) | (NEWLINE statements (KEYWORD:end | if-expr-elif | if-expr-else)) )


if-expr-elif => KEYWORD:elif statement KEYWORD:then
                ((expr (if-expr-elif | if-expr-else?)) | (NEWLINE statements (KEYWORD:end | if-expr-elif | if-expr-else)) )

if-expr-else => KEYWORD:else
                (statement | (NEWLINE statements KEYWORD:end))
        
for-expr => KEYWORD:for IDENTIFIER EQ expr KEYWORD:to expr
            (KEYWORD:step expr)? KEYWORD:then 
            (statement | (NEWLINE statements KEYWORD:end))

while-expr => KEYWORD:while expr KEYWORD:then
              (statement | (NEWLINE statements KEYWORD:end))

func-expr => KEYWORD:func IDENTIFIER?
             LPAREN (IDENTIFIER (COMMA IDENTIFIER)*)? RPAREN
             ((ARROW expr) | (NEWLINE statements KEYWORD:end))
```

- 词法分析部分
  - 添加注释部分 √
  - 添加换行部分 √
- 语法分析部分
  - 添加 statements 和 statement 的解析 √
  - 修改 if elif else for while func 等非终结符节点的编写(添加了多行编程, 现在需要以 end 作为结束符) √
  - 添加 ParseResult 的试错功能 √
  - 添加 ReturnNode, ContinueNode, BreakNode √
- 解释器部分
  - 修改 RTResult , 添加循环continue, break, 和函数return的能力 √
  - 修改处理各种节点的能力 √