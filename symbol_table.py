"""
符号表, 用于存储变量的值, 放在上下文Context对象中
"""


class SymbolTable(object):

    def __init__(self, parent=None):
        # 符号表, 底层是一个 dict
        self.symbol = {}
        # 父级的符号表
        self.parent = parent

    def get(self, name: str):
        """
        用于获取变量的值
        :param name: 变量名
        :return:
        """

        value = self.symbol.get(name, None)
        # 在父作用域找找看
        if value is None and self.parent:
            return self.parent.get(name)

        return value

    def set(self, name: str, value):
        """
        变量赋值
        :param name: 变量名
        :param value: 变量值
        """
        self.symbol[name] = value

    def remove(self, name: str):
        """
        删除变量
        :param name:
        :return:
        """
        del self.symbol[name]
