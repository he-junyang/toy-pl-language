from basic import run
import sys  # 导入sys模块

sys.setrecursionlimit(2000)  # 将默认的递归深度修改为3000


def test():
    def func(text):
        result, error = run('<stdin>', text)
        if error:
            print(error.as_string())
        elif result:
            print(result)

    command = '-10 * 3 + (2 + 1.0) / 4 - 3'  # -32.25
    func(command)
    command = '--5'  # 5
    func(command)
    command = '2^8'  # 256
    func(command)
    command = 'var a = var b = var c = 1'  # 1
    func(command)
    command = 'var x = 4 + a - (1 * b)'  # 4
    func(command)
    command = '3 >= 3'  # 1
    func(command)
    command = '6.0 > 7'  # 0
    func(command)
    command = '3 < 1'  # 0
    func(command)
    command = '3 <= 2'  # 0
    func(command)
    command = '1 == 1 and 3 > 2'  # 1
    func(command)
    command = '(1 == 1) and (3 > 2)'  # 1
    func(command)
    command = 'var a = (1 == 1) and (3 > 2)'  # 1
    func(command)
    command = 'if 3 < 1 then 40 elif 5 > 4.0 then 50 else 60'  # ['判断值为 False', 50, 60]
    func(command)
    command = 'if 3 < 1 then var a = 2 elif 5 > 6 then var c = -4'  # ['判断值为 False', '判断值为 False']
    func(command)
    command = 'var i = 0'  # 0
    func(command)
    command = 'while i < 10 then var i = i + 1'  # ['while 循环值 : ', 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    func(command)
    command = 'i'  # 10
    func(command)
    command = 'var res = 1'  # 1
    func(command)
    command = 'for i = 1 to 10 step 2 then var res = res + i'  # ['for 循环值 : ', 2, 5, 10, 17, 26]
    func(command)
    command = 'res'  # 26
    func(command)
    command = 'func add(a,b) -> a + b'  # <function add>
    func(command)
    command = 'add((1 + 3) / 4.0 , 23)'  # 24.0
    func(command)
    command = 'var s = "123"'  # 123
    func(command)
    command = 'var ss = "123 \t 456"'  # 123 	 456
    func(command)
    command = 'var sss = "123 \t 456 \n 789"'  # 123 	 456 \n789
    func(command)
    # 测试 list
    command = '[1,2,3]'  # [1, 2, 3]
    func(command)
    command = '[1,2,3] + 4'  # [1, 2, 3, 4]
    func(command)
    command = '[1,2,3] + [4,5,6]'  # [1, 2, 3, [4, 5, 6]]
    func(command)
    command = '[1,2,3] / 0'  # 1
    func(command)
    command = '[1,2,3] * [4,5,6]'  # [1, 2, 3, 4, 5, 6]
    func(command)
    command = '[1,2,3] - 0'  # [2, 3]
    func(command)
    # 测试内建函数
    command = 'print(10)'  # 输入 10
    func(command)
    command = 'var a = append([1,2,3], 4)'  # a => [1,2,3,4]
    func(command)
    command = 'var a = pop([1,2,3], 0)'  # a => 1
    func(command)
    command = 'var a = extend([1,2,3], [4,5])'  # a => [1,2,3,4,5]
    func(command)
    # 测试 break, continue, return 关键字
    command = r'''
            var r1 = var r2 = 0;
            for i = 1 to 10 step 2 then;
                var r1 = r1 + i;
                var r2 = r2 - i;
            end;
            r1 ; r2; i
            '''
    func(command)  # [0, None, 25, -25, 9]
    command = '''
    var r1 = var r2 = 0;
    while i < 10 then;
        var r1 = r1 + i;
        var r2 = r2 - i;
        var i = i + 1;
    end;
    r1 ; r2
    '''
    func(command)  # 0, 0, 9, -9
    command = '''
    func add(a, b)
        var res = 0
        var a = a + 1
        var res = a + b
        return res
    end

    var a = add(1,3)
    var b = add(3,4)
    print(a)
    '''
    func(command)  # 5
    command = '''
    var a = []
    # 你好
    var i = 0
    while i < 10 then
        var i = i + 1
        if i == 4 then
            continue
        end
        if i == 7 then
            break
        end
        var a = append(a, i)
    end
    print(a)
    '''
    func(command)  # 1, 2, 3, 5, 6

    command = '''
        var a = add(1,3)
        print(a)
        print(a)
    '''
    func(command)  # 5
    command = '''
    func get(n)
        if n <=0 then
            return 0
        elif n == 1 then
            return 1
        end
        return get(n-1) + get(n-2)
    end
    print(get(9))
    print(get(8))
    get(10)
    '''
    func(command)
    command = '''
# Toy Programming language 使用测试

# 递归
func Fib(n)
    if n <= 0 then
        return 0
    elif n == 1 then
        return 1
    else
        return Fib(n-1) + Fib(n-2)
    end
end

# 将斐波那契数列存入列表中
func Fib_Res(n)
    var res_list = []
    for i = 1 to n+1 then
        var res_list = append(res_list, Fib(i))
    end
    return res_list
end

print(Fib_Res(10))
    '''
    func(command)


test()
