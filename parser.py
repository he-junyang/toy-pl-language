"""
语法分析器文件
"""
from tokens import *
from error import *
from ast_node import *
from result import *


class Parser(object):
    """语法解析器"""

    def __init__(self, tokens: list):
        self.current_tok: Token = None  # 当前 token
        self.tokens = tokens  # 词法分析器解析的 tokens
        self.tok_idx = -1  # 当前数组下标
        self.advance()  # 获取下一个token

    def advance(self) -> Token:
        """
        获取下一个 token
        :return:
        """
        self.tok_idx += 1
        if self.tok_idx < len(self.tokens):
            self.current_tok = self.tokens[self.tok_idx]
        return self.current_tok

    def rollback(self, amount=1) -> Token:
        """
        回滚 token
        :param amount:
        :return:
        """
        self.tok_idx -= amount
        if 0 <= self.tok_idx < len(self.tokens):
            self.current_tok = self.tokens[self.tok_idx]
        return self.current_tok

    def parse(self) -> ParserResult:
        """
        构建抽象语法树
        :return:
        """
        # 第一个非终结符是 expr, 从 expr 开始解析
        res = self.statements()

        if res.error and self.current_tok.type != TT_EOF:
            # 报错
            return res.failure(
                InvalidSyntaxError(self.current_tok.pos_start,
                                   self.current_tok.pos_end,
                                   "期望 'EOF' 文件终结符"))
        return res

    """
    非终结符的处理
    """

    def statements(self):
        """
        statements => NEWLINE* statement (NEWLINE+ statement)* NEWLINE*
        :return:
        """
        res = ParserResult()
        statements = []  # 存储需要执行的 statement
        pos_start = self.current_tok.pos_start.copy()

        while self.current_tok.type == TT_NEWLINE:
            res.register_advancement()
            self.advance()

        statement = res.register(self.statement())
        if res.error:
            return res
        statements.append(statement)

        more_statement = True
        while True:
            newline_count = 0
            while self.current_tok.type == TT_NEWLINE:
                res.register_advancement()
                self.advance()
                newline_count += 1
            if newline_count == 0:  # 下一个元素不是换行, 说明ok了
                more_statement = False
            if not more_statement:
                break

            statement = res.try_register(self.statement())
            # 失败了就是None, 成功了就是正常的 Node
            if not statement:
                # ??? 为什么使用 try_register 而不是 register ?
                # 仔细观察规则： statements  :NEWLINE* statement (NEWLINE+ statement)* NEWLINE*
                # 其中 (NEWLINE+ statement)* NEWLINE* 表示：
                # (NEWLINE+ statement)*  (NEWLINE+ statement)整体可能出现0次或多次，如果至少出现一次，那么其中的NEWLINE可以出现1次或多次而其中的statement则比如出现
                # 规则的另外一部分，NEWLINE* 表示NEWLINE可能出现0次或多次
                # 注意，(NEWLINE+ statement)* 与 NEWLINE*  都以 NEWLINE 开头，也就说，上一个token为NEWLINE，那么它下一个token是否为statement，无法判断
                # 此时就需要做尝试，通过try_register方法尝试解析，如果解析失败了，则回退
                self.rollback(res.to_reverse_count)
                more_statement = False
                continue
            statements.append(statement)
        # 多行逻辑返回list
        return res.success(ListNode(statements, pos_start, self.current_tok.pos_end.copy()))

    def statement(self):
        """
        statement => KEYWORD:return expr?
                  => KEYWORD:continue
                  => KEYWORD:break
                  => expr
        :return:
        """
        res = ParserResult()
        pos_start = self.current_tok.pos_start.copy()
        if self.current_tok.matches(TT_KEYWORDS, 'return'):
            res.register_advancement()
            self.advance()

            # KEYWORD:return expr? 表示expr出现0次或1次
            # 试错, 可能下一个并不是 expr, 是换行什么的 ,如果不是的话, 就回滚
            expr = res.try_register(self.expr())
            if not expr:
                self.rollback(res.to_reverse_count)
            return res.success(ReturnNode(expr, pos_start, self.current_tok.pos_start.copy()))

        if self.current_tok.matches(TT_KEYWORDS, 'continue'):
            res.register_advancement()
            self.advance()
            return res.success(ContinueNode(pos_start, self.current_tok.pos_start.copy()))

        if self.current_tok.matches(TT_KEYWORDS, 'break'):
            res.register_advancement()
            self.advance()
            return res.success(BreakNode(pos_start, self.current_tok.pos_start.copy()))

        expr = res.register(self.expr())
        if res.error:
            return res.failure(InvalidSyntaxError(
                self.current_tok.pos_start, self.current_tok.pos_end,
                "Expected 'return', 'continue', 'break', 'var', 'if', 'for', 'where', 'fun', int, float, identifier, "
                "'+', '-', '(', '[' or 'not'"
            ))

        return res.success(expr)

    def expr(self) -> ParserResult:
        """
        expr => KEYWORD:var IDENTIFIER EQ expr
             => comp-expr (( KEYWORD:and | KEYWORD:or) comp-expr)*
        :return:
        """
        res = ParserResult()

        # 是变量
        if self.current_tok.matches(TT_KEYWORDS, 'var'):
            # 这个处理结果已经占了一个 token 了
            res.register_advancement()
            self.advance()

            # B 不是变量名就报错
            if self.current_tok.type is not TT_IDENTIFIER:
                return res.failure(InvalidSyntaxError(
                    self.current_tok.pos_start, self.current_tok.pos_end,
                    "期望 '变量' , 结果不符合预期"
                ))

            var_name = self.current_tok
            res.register_advancement()
            self.advance()  # 下标向前走

            if self.current_tok.type is not TT_EQ:
                return res.failure(InvalidSyntaxError(
                    self.current_tok.pos_start, self.current_tok.pos_end,
                    "期望 '=' , 结果不符合预期"
                ))
            res.register_advancement()
            self.advance()

            expr = res.register(self.expr())
            if res.error:
                return res

            return res.success(VarAssignNode(var_name, expr))
        else:
            node = res.register(self.bin_op(self.com_expr, ((TT_KEYWORDS, 'and'), (TT_KEYWORDS, 'or'))))
            if res.error:
                return res.failure(InvalidSyntaxError(
                    self.current_tok.pos_start, self.current_tok.pos_end,
                    "期望 'var', 'int', 'float', '=', '+', '-', '(', 'and', 'or' 或 'not', 结果不符合预期"
                ))
            return res.success(node)

    def com_expr(self):
        """
        comp-expr => KEYWORD:not comp-expr # 取反
                  => arith-expr ((EE | LT | GT | LTE | GTE | NE) arith-expr)*
        :return:
        """
        res = ParserResult()

        if self.current_tok.matches(TT_KEYWORDS, 'not'):
            op_token = self.current_tok
            res.register_advancement()
            self.advance()
            node = res.register(self.com_expr())
            if res.error:
                return res
            return res.success(UnaryOpNode(op_token, node))
        else:
            node = res.register(self.bin_op(self.arith_expr, (TT_EE, TT_LT, TT_GT, TT_LTE, TT_GTE, TT_NE)))
            if res.error:
                return res.failure(InvalidSyntaxError(
                    self.current_tok.pos_start, self.current_tok.pos_end,
                    "期望  '!=', '<', '>', '<=', '>=' 或 '==' 结果不符合预期"
                ))

            return res.success(node)

    def arith_expr(self):
        """
        arith-expr => term ((PLUS | MINUS) term)*
        :return:
        """
        return self.bin_op(self.term, (TT_PLUS, TT_MINUS))

    def term(self) -> ParserResult:
        """
        term => factor(( MUL | DIV) factor)*
        :return:
        """
        # 相同的逻辑, 一起处理了
        return self.bin_op(self.factor, (TT_MUL, TT_DIV))

    def bin_op(self, func_a, ops, func_b=None) -> ParserResult:
        if func_b is None:
            func_b = func_a
        res = ParserResult()
        left = res.register(func_a())
        if res.error:
            return res
        while self.current_tok.type in ops or (self.current_tok.type, self.current_tok.value) in ops:
            # 获取中间那个运算符
            op_token = self.current_tok
            # 进入下一个 token
            res.register_advancement()
            self.advance()
            # 获取右边的操作数
            right = res.register(func_b())
            if res.error:
                return res
            left = BinOpNode(left, op_token, right)
        return res.success(left)

    def factor(self) -> ParserResult:
        """
        factor => (PLUS | MINUS) factor # 正数与负数
               => power
        :return:
        """
        res = ParserResult()
        token = self.current_tok
        if token.type in (TT_PLUS, TT_MINUS):
            res.register_advancement()
            self.advance()
            factor = res.register(self.factor())
            if res.error:
                return res
            return res.success(UnaryOpNode(token, factor))
        else:
            return self.power()

    def power(self) -> ParserResult:
        """
        power => atom (POW factor)*
        """
        return self.bin_op(self.call, (TT_POW,), self.factor)

    def atom(self) -> ParserResult:
        """
        atom => INT | FLOAT | IDENTIFIER
             => LPAREN expr RPAREN
             => if-expr
        """
        res = ParserResult()
        token = self.current_tok

        if token.type in (TT_INT, TT_FLOAT):
            res.register_advancement()
            self.advance()
            return res.success(NumberNode(token))
        elif token.type == TT_STRING:
            res.register_advancement()
            self.advance()
            return res.success(StringNode(token))
        elif token.type == TT_IDENTIFIER:
            res.register_advancement()
            self.advance()
            return res.success(VarAccessNode(token))
        elif token.type == TT_LSQUARE:
            list_expr = res.register(self.list_expr())
            if res.error:
                return res
            return res.success(list_expr)
        elif token.type == TT_LPAREN:
            res.register_advancement()
            self.advance()
            expr = res.register(self.expr())
            if res.error:
                return res
            if self.current_tok.type == TT_RPAREN:
                res.register_advancement()
                self.advance()
                return res.success(expr)
            else:
                return res.failure(InvalidSyntaxError(
                    self.current_tok.pos_start, self.current_tok.pos_end,
                    "期望 ')', 结果与预期不符"
                ))
        elif token.matches(TT_KEYWORDS, 'if'):
            if_expr = res.register(self.if_expr())
            if res.error:
                return res
            return res.success(if_expr)
        elif token.matches(TT_KEYWORDS, 'for'):
            for_expr = res.register(self.for_expr())
            if res.error:
                return res
            return res.success(for_expr)
        elif token.matches(TT_KEYWORDS, 'while'):
            while_expr = res.register(self.while_expr())
            if res.error:
                return res
            return res.success(while_expr)
        elif token.matches(TT_KEYWORDS, 'func'):
            func_expr = res.register(self.func_expr())
            if res.error:
                return res
            return res.success(func_expr)
        return res.failure(InvalidSyntaxError(
            token.pos_start, token.pos_end,
            "期望 'int', 'float', 'identifier', '+', '-', '*', '/'. '(' 或 ')', 结果与预期不符"
        ))

    def if_expr(self) -> ParserResult:
        """
        if-expr => KEYWORD:if statement KEYWORD:then
                  (( expr (if-expr-elif | if-expr-c?) ) | (NEWLINE statements (KEYWORD:end | if-expr-elif | if-expr-else)) )
        :return: (cases, else_case) , 用 ParseResult 包装
        """
        res = ParserResult()
        # cases: list[tuple(condition, statements , 是否单行, False 为单行)]
        # else_case: tuple(statements , 是否单行, False 为单行)
        cases, else_case = res.register(self.if_expr_cases('if'))
        if res.error:
            return res
        return res.success(IfNode(cases, else_case))

    def if_expr_elif(self) -> ParserResult:
        """
        if-expr-elif => KEYWORD:elif statement KEYWORD:then
                        ((expr (if-expr-elif | if-expr-else?)) | (NEWLINE statements (KEYWORD:end | if-expr-elif | if-expr-else)) )
        :return: (cases, else_case) , 用 ParseResult 包装
        """
        return self.if_expr_cases('elif')

    def if_expr_else(self) -> ParserResult:
        """
        if-expr-else => KEYWORD:else (statement | (NEWLINE statements KEYWORD:end))
        :return: 如果没有 else 部分, 返回 ParserResult().success(None),
        如果有, 返回 ParserResult().success(else_case, bool:是否在一行, False为在一行上)
        """
        res = ParserResult()
        else_case = None

        if self.current_tok.matches(TT_KEYWORDS, 'else'):
            res.register_advancement()
            self.advance()

            if self.current_tok.type == TT_NEWLINE:  # 是空行
                res.register_advancement()
                self.advance()

                statements = res.register(self.statements())
                if res.error:
                    return res

                else_case = (statements, True)
                if self.current_tok.matches(TT_KEYWORDS, 'end'):
                    res.register_advancement()
                    self.advance()

                else:
                    return res.failure(InvalidSyntaxError(
                        self.current_tok.pos_start, self.current_tok.pos_end,
                        "期望 'end', 结果与期望不符"
                    ))
            else:
                expr = res.register(self.statement())
                if res.error:
                    return res
                else_case = (expr, False)

        return res.success(else_case)

    def if_expr_cases(self, case_keyword: str) -> ParserResult:
        """
        由于 if-expr 和 if-expr-elif 文法高度相似, 我们将代码复用
        :return:
        """
        res = ParserResult()
        cases = []
        else_case = None

        # 判断关键字是为 if 或 elif
        if not self.current_tok.matches(TT_KEYWORDS, case_keyword):
            return res.failure(InvalidSyntaxError(
                self.current_tok.pos_start, self.current_tok.pos_end,
                f"期望 '{case_keyword}', 结果与实际不符"
            ))

        res.register_advancement()
        self.advance()

        condition = res.register(self.expr())
        if res.error:
            return res

        if not self.current_tok.matches(TT_KEYWORDS, 'then'):
            return res.failure(InvalidSyntaxError(
                self.current_tok.pos_start, self.current_tok.pos_end,
                "期望 'then', 结果与预期不符"
            ))

        res.register_advancement()
        self.advance()
        if self.current_tok.type == TT_NEWLINE:
            res.register_advancement()
            self.advance()
            statements = res.register(self.statements())
            if res.error:
                return res
            cases.append((condition, statements, True))
            if self.current_tok.matches(TT_KEYWORDS, 'end'):
                res.register_advancement()
                self.advance()
            else:
                elif_cases, else_case = res.register(self.if_expr_elif_or_else())
                if res.error:
                    return res
                cases.extend(elif_cases)
        else:
            expr = res.register(self.statements())
            if res.error:
                return res

            cases.append((condition, expr, False))
            elif_cases, else_case = res.register(self.if_expr_elif_or_else())
            cases.extend(elif_cases)
        return res.success((cases, else_case))

    def if_expr_elif_or_else(self) -> ParserResult:
        """
        (if-expr-elif | if-expr-else?)
        用于简化文法解析
        :return:
        """
        res = ParserResult()
        cases, else_case = [], None

        if self.current_tok.matches(TT_KEYWORDS, 'elif'):
            all_cases = res.register(self.if_expr_elif())
            if res.error:
                return res
            cases, else_case = all_cases
        else:
            else_case = res.register(self.if_expr_else())
            if res.error:
                return res

        return res.success((cases, else_case))

    def for_expr(self):
        """
        for-expr => KEYWORD:for IDENTIFIER EQ expr KEYWORD:to expr
                    (KEYWORD:step expr)? KEYWORD:then
                    (statement | (NEWLINE statements KEYWORD:end))
        :return:
        """
        res = ParserResult()

        if not self.current_tok.matches(TT_KEYWORDS, 'for'):
            return res.failure(InvalidSyntaxError(
                self.current_tok.pos_start, self.current_tok.pos_end,
                "期望 'for', 结果与预期不符"
            ))

        res.register_advancement()
        self.advance()

        if self.current_tok.type != TT_IDENTIFIER:
            return res.failure(InvalidSyntaxError(
                self.current_tok.pos_start, self.current_tok.pos_end,
                "期望标识符, 结果与预期不符"
            ))

        var_name = self.current_tok  # 获取变量名
        res.register_advancement()
        self.advance()

        if self.current_tok.type != TT_EQ:
            return res.failure(InvalidSyntaxError(
                self.current_tok.pos_start, self.current_tok.pos_end,
                "期望 '=', 结果与预期不符"
            ))

        res.register_advancement()
        self.advance()
        start_value = res.register(self.expr())  # for 循环起始值
        if res.error:
            return res

        if not self.current_tok.matches(TT_KEYWORDS, 'to'):
            return res.failure(InvalidSyntaxError(
                self.current_tok.pos_start, self.current_tok.pos_end,
                "期望 'to', 结果与预期不符"
            ))

        res.register_advancement()
        self.advance()
        end_value = res.register(self.expr())  # for 循环起始值
        if res.error:
            return res

        step_value = None  # 跳过的个数
        if self.current_tok.matches(TT_KEYWORDS, 'step'):
            res.register_advancement()
            self.advance()
            step_value = res.register(self.expr())
            if res.error:
                return res

        if not self.current_tok.matches(TT_KEYWORDS, 'then'):
            return res.failure(InvalidSyntaxError(
                self.current_tok.pos_start, self.current_tok.pos_end,
                "期望 'then', 结果与预期不符"
            ))

        res.register_advancement()
        self.advance()

        if self.current_tok.type == TT_NEWLINE:
            res.register_advancement()
            self.advance()

            body = res.register(self.statements())
            if res.error:
                return res
            if not self.current_tok.matches(TT_KEYWORDS, 'end'):
                return res.failure(InvalidSyntaxError(
                    self.current_tok.pos_start, self.current_tok.pos_end,
                    "期望 'end', 结果与预期不符"
                ))

            res.register_advancement()
            self.advance()

            return res.success(ForNode(var_name, start_value, end_value, step_value, body, True))

        body = res.register(self.statement())  # for循环体内的内容
        if res.error:
            return res

        return res.success(ForNode(var_name, start_value, end_value, step_value, body, False))

    def while_expr(self):
        """
        while-expr => KEYWORD:while expr KEYWORD:then expr
                      (statement | (NEWLINE statements KEYWORD:end))
        :return:
        """
        res = ParserResult()

        if not self.current_tok.matches(TT_KEYWORDS, 'while'):
            return res.failure(InvalidSyntaxError(
                self.current_tok.pos_start, self.current_tok.pos_end,
                "期望 'while', 结果与预期不符"
            ))

        res.register_advancement()
        self.advance()
        condition = res.register(self.expr())
        if res.error:
            return res

        if not self.current_tok.matches(TT_KEYWORDS, 'then'):
            return res.failure(InvalidSyntaxError(
                self.current_tok.pos_start, self.current_tok.pos_end,
                "期望 'then', 结果与预期不符"
            ))

        res.register_advancement()
        self.advance()
        if self.current_tok.type == TT_NEWLINE:
            res.register_advancement()
            self.advance()
            body = res.register(self.statements())
            if res.error:
                return res
            res.register_advancement()
            self.advance()

            return res.success(WhileNode(condition, body, True))
        body = res.register(self.statement())
        if res.error:
            return res

        return res.success(WhileNode(condition, body, False))

    def func_expr(self):
        """
        func-expr => KEYWORD:func IDENTIFIER?
                     LPAREN (IDENTIFIER (COMMA IDENTIFIER)*)? RPAREN
                     ((ARROW expr) | (NEWLINE statements KEYWORD:end))
        :return:
        """
        res = ParserResult()
        if not self.current_tok.matches(TT_KEYWORDS, 'func'):
            return res.failure(InvalidSyntaxError(
                self.current_tok.pos_start, self.current_tok.pos_end,
                "期望 'func', 结果与预期不符"
            ))

        res.register_advancement()
        self.advance()

        # 匹配函数名
        var_name_token: Token = None
        if self.current_tok.type == TT_IDENTIFIER:
            # 名字对应的 token
            var_name_token = self.current_tok
            res.register_advancement()
            self.advance()
            # func a()
            if self.current_tok.type != TT_LPAREN:
                return res.failure(InvalidSyntaxError(
                    self.current_tok.pos_start, self.current_tok.pos_end,
                    "期望 '(', 结果与预期不符"
                ))
        else:
            var_name_token: Token = None
            # func ()
            if self.current_tok.type != TT_LPAREN:
                return res.failure(InvalidSyntaxError(
                    self.current_tok.pos_start, self.current_tok.pos_end,
                    "期望 '标识符' 或 '(', 结果与预期不符"
                ))
        res.register_advancement()
        self.advance()
        # 函数的参数
        arg_name_tokens = []

        if self.current_tok.type == TT_IDENTIFIER:
            arg_name_tokens.append(self.current_tok)
            res.register_advancement()
            self.advance()

            while self.current_tok.type == TT_COMMA:
                res.register_advancement()
                self.advance()

                if self.current_tok.type != TT_IDENTIFIER:
                    return res.failure(InvalidSyntaxError(
                        self.current_tok.pos_start, self.current_tok.pos_end,
                        "期望 '标识符', 结果与预期不符"
                    ))

                arg_name_tokens.append(self.current_tok)
                res.register_advancement()
                self.advance()

            if self.current_tok.type != TT_RPAREN:
                return res.failure(InvalidSyntaxError(
                    self.current_tok.pos_start, self.current_tok.pos_end,
                    "期望 ')', 结果与预期不符"
                ))
        else:
            if self.current_tok.type != TT_RPAREN:
                return res.failure(InvalidSyntaxError(
                    self.current_tok.pos_start, self.current_tok.pos_end,
                    "期望 ')', 结果与预期不符"
                ))
        res.register_advancement()
        self.advance()
        # 现在已经匹配完了 func a(1,2,3) ,可以匹配箭头了
        if self.current_tok.type == TT_ARROW:
            res.register_advancement()
            self.advance()
            # 解析函数体中的逻辑，获得该函数的返回值
            node_to_return = res.register(self.expr())
            if res.error: return res
            # should_auto_return设置为True，表示自动返回，此时的函数为一行函数，不需要通过return关键字返回内容
            # func add(a,b) -> a + b  => add函数会返回 a+b 的结果
            return res.success(FuncNode(var_name_token, arg_name_tokens, node_to_return, True))

        if self.current_tok.type != TT_NEWLINE:
            return res.failure(InvalidSyntaxError(
                self.current_tok.pos_start, self.current_tok.pos_end,
                "期望 '->' or '换行', 结果与预期不符"
            ))

        res.register_advancement()
        self.advance()

        body = res.register(self.statements())
        if res.error:
            return res

        if not self.current_tok.matches(TT_KEYWORDS, 'end'):
            return res.failure(InvalidSyntaxError(
                self.current_tok.pos_start, self.current_tok.pos_end,
                "期望 'end', 结果与预期不符"
            ))

        res.register_advancement()
        self.advance()

        return res.success(FuncNode(var_name_token, arg_name_tokens, body, False))

    def call(self):
        """
        call => atom (LPAREN (expr (COMMA expr)*)? RPAREN)? # 调用函数或者调用变量, 如 atop(1,2) , atom
        :return:
        """
        res = ParserResult()

        atom = res.register(self.atom())
        if res.error:
            return res

        if self.current_tok.type == TT_LPAREN:
            res.register_advancement()
            self.advance()
            arg_nodes = []

            # add()
            if self.current_tok.type == TT_RPAREN:
                res.register_advancement()
                self.advance()
            # add(1,2,3)
            else:
                arg_nodes.append(res.register(self.expr()))
                if res.error:
                    return res

                while self.current_tok.type == TT_COMMA:
                    res.register_advancement()
                    self.advance()
                    arg_nodes.append(res.register(self.expr()))
                    if res.error:
                        return res

                if self.current_tok.type != TT_RPAREN:
                    return res.failure(InvalidSyntaxError(
                        self.current_tok.pos_start, self.current_tok.pos_end,
                        "期望 ')', 结果不符合预期"
                    ))
                res.register_advancement()
                self.advance()
            return res.success(CallNode(atom, arg_nodes))
        return res.success(atom)

    def list_expr(self):
        """
        list-expr =>  LSQUARE (expr (COMMA expr)*)? RSQUARE # [1,2,3]
        :return:
        """
        res = ParserResult()
        element_nodes = []  # 用于存放列表中的元素
        pos_start = self.current_tok.pos_start.copy()

        if self.current_tok.type != TT_LSQUARE:
            return res.failure(InvalidSyntaxError(
                self.current_tok.pos_start, self.current_tok.pos_end,
                "期望 '[', 结果不符合预期"
            ))

        res.register_advancement()
        self.advance()

        if self.current_tok.type == TT_RSQUARE:
            res.register_advancement()
            self.advance()
        else:
            element_nodes.append(res.register(self.expr()))
            if res.error:
                return res

            while self.current_tok.type == TT_COMMA:
                res.register_advancement()
                self.advance()
                element_nodes.append(res.register(self.expr()))
                if res.error:
                    return res

            if self.current_tok.type != TT_RSQUARE:
                return res.failure(InvalidSyntaxError(
                    self.current_tok.pos_start, self.current_tok.pos_end,
                    "期望 ',' 或 ']', 结果不符合预期"
                ))

            res.register_advancement()
            self.advance()
        return res.success(ListNode(element_nodes, pos_start, self.current_tok.pos_end.copy()))
