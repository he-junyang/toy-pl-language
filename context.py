class Context(object):
    """上下文对象, 默认有一个全局上下文, 每个函数有其自己的上下文"""

    def __init__(self, display_name, parent=None, parent_entry_pos=None):
        """
        :param display_name: 展示的名字
        :param parent:  父上下文, 形成上下文栈
        :param parent_entry_pos:  父位置
        """
        self.display_name = display_name
        self.parent = parent
        self.parent_entry_pos = parent_entry_pos
        self.symbol_table = None
